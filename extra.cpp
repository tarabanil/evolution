#include "Group.h"
#include <cmath>
#include <random>
using namespace std;
std::default_random_engine *generator;
double xrand(int begin, int end){
	std::uniform_real_distribution<double> distribution(begin, end);
	double d = distribution(*generator);
	return d;
}

int round(double d){
	return ((d - floor(d)) >= 0.5) ? floor(d) + 1 : floor(d);
}

void sortInd(double* arr, int* ind, int size){
	double* tempArr;
	tempArr = new double[size];
	for(int i = 0; i < size; ++i){
		tempArr[i] = arr[i];
		ind[i] = i;
	}
	for(int i = 1; i < size; ++i){
		for(int j = 0; j < size - i; ++j){
			if (tempArr[j] < tempArr[j + 1]){
				double temp = tempArr[j];
				tempArr[j] = tempArr[j + 1];
				tempArr[j + 1] = temp;
				temp = ind[j];
				ind[j] = ind[j + 1];
				ind[j + 1] = temp;
			}
		}
	}
	/*
	for(int j = 0; j < size; ++j){
		for(int i = 0; i < size; ++i){
			if (tempArr[j] == arr[i]){
				ind[j] = i;
				break;
			}
		}
	}
	*/
	delete[] tempArr;
}

int findK(int ind, int* K, int size){
	int index = 0;
	int sum = 0;
	for(int j = 0; j < size; ++j){
		sum += K[j];
		if (ind < sum){
			index = j;
			break;
		}
	}
	return index;
}