#include <cstdio>
#include <map>
#include <string>
#include <fstream>
#include <ctime>
#include "Group.h"
#include <conio.h>
#include <csignal>
#include <windows.h>
#include <exception> 

extern std::default_random_engine *generator;

void sayBye(){
	if (generator != NULL)
		delete generator;
	generator = new std::default_random_engine((unsigned long int)time(NULL));
	
	int k = round(xrand(1, 7));
	switch(k){
		case 1:
			printf("Have a nice day!\n");
			break;
		case 2:
			printf("See you later!\n");
			break;
		case 3:
			printf("Goodbye!\n");
			break;
		case 4:
			printf("Bye bye!\n");
			break;
		case 5:
			printf("Adios!\n");
			break;
		case 6:
			printf("Au revoir!\n");
			break;
		case 7:
			printf("���!\n");
			break;
	}
	delete generator;
	Sleep(1500);

}

BOOL CtrlHandler( DWORD fdwCtrlType ) 
{ 
	sayBye();
	return true;
} 


void randPair(int size, int* first, int* second){
	*first = round(xrand(1, size));
	*second = round(xrand(1, size - 1));
	if (*second == *first){
		*second = size;
	}
}

void parse_file(vector<pair<string, string>> &m)
{
	FILE * config = fopen("config.txt", "r");
	char str[128];
	while(!feof(config)) {
		if(fgets(str, 126, config)){
			char c;
			int i = 0;
			c = str[i];
			string first;
			while(c != '='){
				first += c;
				i++;
				if (i > 126){
					break;
				}
				c = str[i];
			}
			if (i > 126){
				break;
			}
			i++;
			c = str[i];
			string second;
			while(c != ' ' || i < 100){
				second += c;
				i++;
				if (i > 126){
					break;
				}
				c = str[i];
			}
			m.push_back(make_pair(first, second));
			//m[first]=second;
		}
	}
	fclose(config);
}

string find(vector<pair<string, string>>&v, string q){
	for(int i = 0; i < v.size(); ++i){
		if (v[i].first.compare(q) == 0){
			return v[i].second;
		}
	}
	return string("");
}


int main(void){
	SetConsoleCtrlHandler( (PHANDLER_ROUTINE) CtrlHandler, TRUE );
	vector<pair<string, string>> k;
	parse_file(k);
	std::fstream fs;
	//fs.open ("4.csv", std::fstream::in | std::fstream::out | std::fstream::trunc);
	string filename = strcmp(find(k, "filename").c_str(), "") ? find(k, "filename") : "test.csv ";
	int len = strlen(filename.c_str());
	char str[128];
	strcpy(str, filename.c_str());
	int w = strcmp(str, "3.csv");
	str[len - 1] = '\0';
	FILE * file = fopen(str, "w+");
	long int seed = atoi(find(k, "seed").c_str());
	if (seed <= 0){
		seed = time(NULL);
	}
	generator = new std::default_random_engine((unsigned long int)seed);
	double b = atof(find(k, "b").c_str());//1;
	double c = atof(find(k, "c").c_str());//0.1;
	int n = atoi(find(k, "n").c_str());//1;//���-�� �����
	int* size;//������� �����
	bool sameSize = (atoi(find(k, "same_size").c_str()) == 1) ? true : false;//true;//���������� �� �������
	bool ranged = (atoi(find(k, "ranged").c_str()) == 1) ? true : false;//true;//���������� �� ���������
	double sGr = atof(find(k, "interGroup").c_str());
	size = new int[n];
	int countInd = 0;//���-�� ���� ������
	for(int i = 0; i < n; ++i){
		if (sameSize){
			size[i] = atoi(find(k, "groupSize").c_str());//100;
		}
		else{
			string s = string("size[") + to_string((_ULonglong)i) + string("]");
			size[i] = atoi(find(k, s.c_str()).c_str());
			if (size[i] <= 0){
				size[i] = atoi(find(k, "groupSize").c_str());
			}
		}
		countInd += size[i];
	}
	int M = atoi(find(k, "M").c_str());
	double p = atof(find(k, "p").c_str());//0;//����������� �������
	int m = (M == 0) ? atoi(find(k, "m").c_str()) : 1;//125;//���-�� �������������
	bool multiM = (M != 0);
	M = (M == 0) ? m : M;
	int generate = atoi(find(k, "g").c_str());//200;//���-�� ���������
	double rOfGroups = atof(find(k, "rOfGroups").c_str());//0.2;//��������� ��������� �����
	Group** groups;
	Group** start_groups;
	Individ** ind;
	double** succPercent;
	succPercent = new double*[n];
	for(int i = 0; i < n; ++i){
		succPercent[i] = new double[n];
	}
	start_groups = new Group*[n]; 
	ind = new Individ*[countInd];
	for(int i = 0; i < n; ++i){
		start_groups[i] = new Group(i, n, size[i], p, ranged, rOfGroups);
	}
	for(; m <= M; m++){
		int sucess_ind = 0;
		double coef = 0;
		int index = 0;
		groups = new Group*[n];
		for(int i = 0; i < n; ++i){
			groups[i] = new Group(*start_groups[i]);
			for(int j = 0; j < size[i]; ++j){
				ind[index] = groups[i]->returnIIndivid(j);
				index++;
			}
		}

		int first = 0;
		int second = 1;

		//��������������
		for(int j = 0; j < generate; ++j){
			sucess_ind = 0;
			for(int i = 0; i < m; ++i){
				randPair(countInd, &first, &second);
				int first_group = ind[first - 1]->getIndexGroup();
				int second_group = ind[second - 1]->getIndexGroup();
				if ((first_group == second_group) && (ind[first - 1]->getK() <= ind[second - 1]->getRep()) ||
					((first_group != second_group) && (ind[first - 1]->getK() <= groups[second_group]->getRep(first_group)))){
					sucess_ind++;
					if (first_group == second_group)
						ind[first - 1]->react(1);
					else
						ind[first - 1]->react(sGr);
					groups[first_group]->win(second_group);
					ind[first - 1]->coop(-c);
					ind[second - 1]->coop(b);
				}
				else{
					if (first_group == second_group)
						ind[first - 1]->react(-1);
					else
						ind[first - 1]->react(-sGr);
					groups[first_group]->defeat(second_group);
				}
			}
			coef = (double)sucess_ind/m;
		
			int* K;
			K = new int[12];
			double avK;
			double avVal;
			double* rep;
			rep = new double[n];
			if (!multiM){
				for(int g = 0; g < n; ++g){
					printf("%d, group %d, k:", j, g);
					fprintf(file, "%d,%d,", j, g);
					//fs << j << "," << g << ",";
					groups[g]->getStatistics(&avK, K, succPercent[g], rep);
					avVal = groups[g]->getAverageValue();
					for(int l = 0; l < 12; ++l){
						printf("%d,", K[l]);
						//fs << K[l] << ",";
						fprintf(file, "%d,", K[l]);
					}
					printf("average K: %lf, reputations:", avK);
					fprintf(file, "%lf,", avK);
					for(int l = 0; l < n; ++l){
						printf(" %lf,", rep[l]);
						fprintf(file, "%lf,", rep[l]);
					}
					printf("average payoff: %lf, ingroup: %lf, intergroup:", avVal, succPercent[g][g]);
					fprintf(file, "%lf,%lf,", avVal, succPercent[g][g]);
					//fs << avK << "," << rep << "," << avVal << "," << succPercent[g] << ",";
					for(int l = 0; l < n - 1; ++l){
						printf("%lf, ", succPercent[g][l]);
						fprintf(file, "%lf,", succPercent[g][l]);
						//fs << succPercent[l] << ",";
					}
					printf("%lf\n", succPercent[g][n - 1]);
					fprintf(file, "%lf\n", succPercent[g][n - 1]);
					//fs << succPercent[n - 1] << endl;
				}
				
				//printf("general: %d %lf\n", sucess_ind, coef);
				//fprintf(file, "%d %lf\n",j, coef);
				
			}
			else
			{
				for(int g = 0; g < n; ++g){
					groups[g]->getStatistics(&avK, K, succPercent[g], rep);
				}
			}
			
			delete[] K;
			delete[] rep;
			if(j != generate - 1){
				for(int l = 0; l < n; ++l){
					groups[l]->nextGeneration();
				}
			}
		}
		if (multiM){
			printf("%d general: %d %lf, ", m, sucess_ind, coef);
			fprintf(file, "%d, %lf, ", m, coef);
			for(int l = 0; l < n; ++l){
				for(int j = 0; j < n; ++j){
					if ((j != n - 1) || (l != n - 1)){ 
						printf("%lf, ", succPercent[l][j]);
						fprintf(file, "%lf, ", succPercent[l][j]);
					}
				}
			}
	
			printf("%lf\n", succPercent[n - 1][n - 1]);
			fprintf(file, "%lf\n", succPercent[n - 1][n - 1]);
		}
		delete[] groups;
	}
	delete[] start_groups;
	delete[] ind;
	delete[] succPercent;
	//fs.close();
	fclose(file);
	printf("Where is seed?\n");
	Sleep(1100);
	printf("Hmmm\n");
	Sleep(1100);
	printf("....\n");
	Sleep(1100);
	printf("Oh!\n");
	Sleep(1100);
	printf("----> %d <----\n", seed);
	delete generator;
	generator = NULL;
	while(1){
		int ch = getch();

		if (ch == 27){
			sayBye();
			break;
		}
	}

	return  0;
}

