#include <cstdlib>
#include <cmath>
#include <random>
using namespace std;

double xrand(int begin, int end);
int round(double d);
void sortInd(double* arr, int* ind, int size);
int findK(int ind, int* K, int size);
class Individ{
private:
	bool ranged;
	double s;//���������
	int k;//���������
	int indexGroup;//������ ������
	double value;//�������� �������� �����
public:
	Individ(int ind, bool r);
	Individ(Individ& i);
	double getS();
	int getK();
	int getIndexGroup();
	double getRep();
	double getValue();
	void nextK(int l);
	void coop(double val);
	void react(double l);
};

class Group{
private:
	bool ranged;//����� �� ������� ���������
	int index;//���������� ����� ������
	int size;//������ ������
	Individ** individs;//����� ������
	double *s;//��������� ������
	int* coop;//����� ����������
	int* success;//����� ��������
	int n;//����� ����� �����
	double p;//����������� �������
	double rOfGroups;//��������� ��������� ����� 
public:
	Group(int ind, int n, int size, double p, bool r, double rGr);
	Group(Group& g);
	void nextGeneration();//���������� ��������� ���������, ����������� �� ��������� �������
	void getStatistics(double* averageK, int* K, double* succPercent, double* rep);//���������� ����������: ������ �� ������������� � � ������� � �� ������, ��������� �� ��������� � ������ �������
	double getAverageValue();
	int getCoop();
	int getSuccess();
	double getRep(int k);
	Individ* returnIIndivid(int i);
	void win(int n);//������� n-��
	void defeat(int n);//��������
	~Group();
};