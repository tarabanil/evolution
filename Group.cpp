#include "Group.h"

Group::Group(int ind, int n, int size, double p, bool r, double rGr):index(ind), n(n), size(size), p(p), ranged(r), rOfGroups(rGr){
	coop = new int[n];
	success = new int[n];
	s = new double[n];
	for(int i = 0; i < n; ++i){
		coop[i] = 0;
		success[i] = 0;
		s[i] = 0;
	}
	individs = new Individ*[size];
	for(int i = 0; i < size; ++i){
		individs[i] = new Individ(ind, ranged);
	}
}

Group::Group(Group& g){
	this->ranged = g.ranged;//����� �� ������� ���������
	this->index = g.index;//���������� ����� ������
	this->size = g.size;//������ ������
	this->n = g.n;
	this->p = g.p;
	this->rOfGroups = g.rOfGroups;
	this->individs = new Individ*[size];//����� ������
	this->s = new double[n];
	this->coop = new int[n];
	this->success = new int[n];
	for(int i = 0; i < n; ++i){
		s[i] = g.s[i];
		coop[i] = g.coop[i];
		success[i] = g.success[i];
	}
	for(int i = 0; i < size; ++i){
		individs[i] = new Individ(*g.individs[i]);
	}
}

//���������� ��������� ���������, ����������� �� ��������� �������
void Group::nextGeneration(){
	double* value;
	double* K;
	value = new double[size];
	K = new double[12];
	for(int i = 0; i < 12; ++i){
		K[i] = 0;
	}
	for(int i = 0; i < size; ++i){
		value[i] = individs[i]->getValue();
	}
	double minVal = value[0];
	for(int i = 0; i < size; ++i){
		if (minVal > value[i])
			minVal = value[i];
	}
	double sum = 0;
	
	if (minVal > 0) 
		minVal = 0;
	for(int i = 0; i < size; ++i){
		value[i] -= minVal;
		sum += value[i];
		K[individs[i]->getK() + 5] += value[i];
	}
	
	//printf("%lf %lf", minVal, sum);
	if (sum == 0){
		for(int i = 0; i < size; ++i){
			if (xrand(0, 1) < p){
				individs[i]->nextK(round(xrand(-5, 6)));
			}
			else{
				individs[i]->nextK(individs[i]->getK());
			}
		}
		delete[] value;
		delete[] K;
		return;
	}
	int* Knumber;
	double* KnumberRemain;
	KnumberRemain = new double[12];
	int r = size;
	Knumber = new int[12];
	for(int i = 0; i < 12; ++i){
		Knumber[i] = floor((size * K[i]) / sum);
		KnumberRemain[i] = (size * K[i]) / sum - floor((size * K[i]) / sum);
		r -= Knumber[i];
	}
	int* SortedIndexes;
	SortedIndexes = new int[12];
	sortInd(KnumberRemain, SortedIndexes, 12);
	for(int i = 0; i < r; ++i){
		Knumber[SortedIndexes[i]]++;
	}
	for(int i = 0; i < size; ++i){
		if (xrand(0, 1) < p){
			individs[i]->nextK(round(xrand(-5, 6)));
		}
		else{
			individs[i]->nextK(findK(i, Knumber, 12) - 5);
		}
	}
	for(int i = 0; i < n; ++i){
		coop[i] = 0;
		success[i] = 0;
	}
	delete[] Knumber;
	delete[] KnumberRemain;
	delete[] SortedIndexes;
	delete[] value;
}


//���������� ����������: ������ �� ������������� � � ������� � �� ������
void Group::getStatistics(double* averageK, int* K, double* succPercent, double* rep){
	double average = 0;
	for(int i = 0; i < 12; ++i){
		K[i] = 0;
	}
	for(int i = 0; i < size; ++i){
		K[individs[i]->getK() + 5]++;
		average += individs[i]->getK();
	}
	*averageK = average / size;
	for(int i = 0; i < n; ++i){
		succPercent[i] = (coop[i] != 0) ? (double)success[i] / coop[i] : -1;
	}
	for(int i = 0; i < n; ++i){
		rep[i] = s[i];
	}
}


double Group::getAverageValue(){
	double av = 0;
	for(int i = 0; i < size; ++i){
		av += individs[i]->getValue();
	}
	return av / size;
}

int Group::getCoop(){
	int c = 0;
	for(int i = 0; i < n; ++i)
		c += coop[i];
	return c;
}

int Group::getSuccess(){
	int suc = 0;
	for(int i = 0; i < n; ++i)
		suc += success[i];
	return suc;
}

double Group::getRep(int k){
	return s[k];
}

Individ* Group::returnIIndivid(int i){
	return individs[i];
}

//������� n-��
void Group::win(int n){
	success[n]++;
	coop[n]++;
	if (n != index){
		s[n] += rOfGroups;
		s[n] = (ranged && s[n] >= 5) ? 5 : ((ranged && s[n] <= -5) ? -5 : s[n]);
	}
}

//��������
void Group::defeat(int n){
	coop[n]++;
	if (n != index){
		s[n] -= rOfGroups;
		s[n] = (ranged && s[n] >= 5) ? 5 : ((ranged && s[n] <= -5) ? -5 : s[n]);
	}
}

Group::~Group(){
	delete[] coop;
	delete[] success;
	delete[] s;
	for(int i = 0; i < size; ++i){
		delete individs[i];
	}
	delete[] individs;
}