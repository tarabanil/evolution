#include "Group.h"


Individ::Individ(int ind, bool r): indexGroup(ind), ranged(r){
	k = round(xrand(-5, 6));
	s = 0;
	value = 0;
}

Individ::Individ(Individ& i){
	ranged = i.ranged;
	k = i.k;
	s = i.s;
	value = i.value;
	indexGroup = i.indexGroup;
}

double Individ::getS(){
	return s;
}

int Individ::getK(){
	return k;
}

int Individ::getIndexGroup(){
	return indexGroup;
}

double Individ::getRep(){
	return s;
}

double Individ::getValue(){
	return value;
}

void Individ::nextK(int l){
	k = l;
	s = 0;
	value = 0;
}

void Individ::coop(double val){
	value += val;
}

void Individ::react(double l){
	s += l;
	s = (ranged && s >= 5) ? 5 : ((ranged && s <= -5) ? -5 : s);
}